package com.atlassian.util.concurrent.bootcamp;

import java.util.Iterator;

import io.atlassian.fugue.Option;

/**
 * Simple singly-linked Stack template.
 */
public class Stack<A> implements Iterable<A> {
  /**
   * Construct an empty stack
   */
  static <A> Stack<A> empty() {
    throw new ImplementMe();
  }

  /**
   * Construct a Stack whose elements are made up of the supplied elements,
   * where the left-most – or first – elements are the ones that will be first
   * out. For instance <code>Stack.&lt;Integer&gt; of(1, 2, 3).peek()</code>
   * will result in <code>1</code>.
   */
  static <A> Stack<A> of(@SuppressWarnings("unchecked") A... as) {
    final Stack<A> s = empty();
    for (int i = as.length - 1; i >= 0; i--) {
      s.push(as[i]);
    };
    return s;
  }

  @Override public Iterator<A> iterator() {
    return new Iterator<A>() {
      @Override public boolean hasNext() {
        throw new ImplementMe();
      }

      @Override public A next() {
        throw new ImplementMe();
      }

      @Override public void remove() {
        throw new UnsupportedOperationException("immutable iterator, do not implement me!");
      }
    };
  }

  /**
   * Non-destructively look at the head element, if there is one.
   */
  public Option<A> peek() {
    throw new ImplementMe();
  }

  /**
   * Pop off the head element, if there is one.
   */
  public Option<A> pop() {
    throw new ImplementMe();
  }

  /**
   * Push a new head element, cannot be null.
   *
   * @return this
   * @throws NullPointerException if the parameter is null
   */
  public Stack<A> push(A a) throws NullPointerException {
    throw new ImplementMe();
  }
}
